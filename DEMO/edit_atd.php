<?php 
    $r='';
    $atd_id ='';
    $emp_id ='';
    $atd_checkin='';
    $atd_checkout='';


    require_once("ketnoi.php");

    if(isset($_GET['r'])){
        $r = $_GET['r'];
    }
    if(isset($_GET['key'])){
        $atd_id = $_GET['key'];

        $sql = "SELECT * FROM attendance WHERE atd_id='".$atd_id."'";
        $result = mysqli_query($conn,$sql);
        $show = mysqli_fetch_assoc($result);
        $atd_id = $show['atd_id'];
        $emp_id=$show['emp_id'];
        $atd_checkin=$show['atd_checkin'];
        $atd_checkout=$show['atd_checkout'];

       
    }

 ?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">
    <title>Edit Attendance</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="css/create-emp.css" rel="stylesheet" media="all">
</head>
<style type="text/css">
    .bg-dark {
          background-image: url("images/login_bg.jpg");
          background-size: cover;
}
</style>

<script type="text/javascript">
    
    function cancel(){
        window.location = "timekeeping_management.php?r=<?php echo $r?>&p=<?php echo $_GET['p'] ?>"
    } 
</script>
<body>
    <div class="page-wrapper bg-dark p-t-100 p-b-50">
        <div class="wrapper wrapper--w900">
            <div class="card card-6">
                <div class="card-heading">
                    <h2 class="title" style="text-align: center;">EDIT ATTENDANCE</h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="update_atd.php?r=<?php echo $r?>&key=<?php echo $atd_id; ?>&p=<?php echo $_GET['p'] ?>" enctype="multipart/form-data">

                        <div class="form-row">
                            <div class="name">Attendance ID <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="atd_id" min="1"
                                placeholder="" value="<?php echo $atd_id?>" required readonly>
                                <small class="label--desc">Can not null.</small>
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="name">Employee ID <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="emp_id" min="1"
                                placeholder="" value="<?php echo $emp_id?>" required readonly>
                                <small class="label--desc">Can not null.</small>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Check-in <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="datetime" name="atd_checkin" value="<?php echo $atd_checkin?>" required>
                            </div>
                        </div> 

                        <div class="form-row">
                            <div class="name">Check-out <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="datetime" name="atd_checkout" value="<?php echo $atd_checkout?>" required>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn--radius-2 btn--blue-2" id="edit" type="submit" name="submit">EDIT</button>
                            <button class="btn btn--radius-2 btn--blue-2" id="back" style="background-color: grey;" onclick="cancel()" type="button">CANCEL</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="js/jquery.min.js"></script>


    <!-- Main JS-->
    <script src="js/global.js"></script>


</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->