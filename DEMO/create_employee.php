<?php 
    $r='';
    if(isset($_GET['r'])){
        $r = $_GET['r'];
    }
 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">
    <title>Create New Employee Profile</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="css/create-emp.css" rel="stylesheet" media="all">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<style type="text/css">
    .bg-dark {
          background-image: url("images/login_bg.jpg");
          background-size: cover;
}
</style>

<script type="text/javascript">
    
    function cancel(){
        window.location = "employee_management.php?r=<?php echo $r?>&p=<?php echo $_GET['p'] ?>";
    } 
</script>
<body>
    <div class="page-wrapper bg-dark p-t-100 p-b-50">
        <div class="wrapper wrapper--w900">
            <div class="card card-6">
                <div class="card-heading">
                    <h2 class="title" style="text-align: center;">CREATE NEW EMPLOYEE PROFILE</h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="add_emp.php?r=<?php echo $r?>&p=<?php echo $_GET['p'] ?>" enctype="multipart/form-data">
                        <div class="form-row">
                            <div class="name">Employee ID</div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="emp_id" min="0"
                                placeholder="" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Account ID</div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="acc_id" min="1"
                                placeholder="" >
                                <small class="label--desc">Can NULL.</small>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Full name</div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="emp_name"
                                placeholder="Full name of employee"  required>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Avatar</div>
                            <div class="value">
                                <div class="input-group js-input-file">
                                    <input class="input-file" type="file" name="emp_avatar" id="file">
                                    <label class="label--file" for="file">Choose file</label>
                                    <span class="input-file__info">No file chosen</span>
                                </div>
                                <div class="label--desc">Upload your photo. Max file size 10 MB.</div>
                            </div>
                        </div>

                       <div class="form-row">
                            <div class="name">Gender</div>
                            <div class="value">

                                <select class="input--style-6" style="width: 100%; height: 40px" name="emp_gender">
                                  <option value="Male">Male</option>
                                  <option value="Female">Female</option>
                                  <option value="Other">Other</option>
                                </select>
                                
                            </div>
                        </div> 

                         <div class="form-row">
                            <div class="name">Phone</div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="emp_phone"
                                placeholder="0000000000"  required>
                            </div>
                        </div> 

                        <div class="form-row">
                            <div class="name">Email</div>
                            <div class="value">
                                <input class="input--style-6" type="Email" name="emp_email"
                                placeholder="nvawork@gmail.com"  required>
                            </div>
                        </div> 

                        <div class="form-row">
                            <div class="name">Date Of Birth</div>
                            <div class="value">
                                <input class="input--style-6" type="date" name="emp_birth"  required>
                            </div>
                        </div> 

                        <div class="form-row">
                            <div class="name">No. Identification</div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="emp_no_identification"
                                placeholder="">
                            </div>
                        </div> 
                        
                        <div class="form-row">
                            <div class="name">No. Passport</div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="emp_no_passport"
                                placeholder="">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Marital Status</div>
                            <div class="value">

                                <select class="input--style-6" style="width: 100%; height: 40px" name="emp_marital_status">
                                  <option value="Married">Married</option>
                                  <option value="Divorced">Divorced</option>
                                  <option value="Single">Single</option>
                                  <option value="Widower">Widower</option>
                                  <option value="Other">Other</option>
                                </select>
                                
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Education Certificate Level</div>
                            <div class="value">

                                <select class="input--style-6" style="width: 100%; height: 40px" name="emp_education_level">
                                  <option value="Graduate">Graduate</option>
                                  <option value="Bachelor">Bachelor</option>
                                  <option value="Master">Master</option>
                                  <option value="Doctor">Doctor</option>
                                  <option value="Other">Other</option>
                                </select>
                                
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Education Field Of Study</div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="emp_education_field"
                                placeholder="">
                            </div>
                        </div> 

                        <div class="form-row">
                            <div class="name">Education School</div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="emp_education_school"
                                placeholder="">
                            </div>
                        </div> 

                        <div class="form-row">
                            <div class="name">Manager ID</div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="emp_manager" min="0"
                                placeholder="">
                                <small class="label--desc">Choose 0 if you do not have a manager</small>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Coach ID</div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="emp_coach" min="0"
                                placeholder="">
                                <small class="label--desc">Choose 0 if you do not have a coach</small>
                            </div>
                        </div>
                        <div class="card-footer">
                        <button class="btn btn--radius-2 btn--blue-2" type="submit" name="submit" style="display: inline-block; margin-left: 0px;">CREATE</button>
                
                        <button class="btn btn--radius-2 btn--blue-2" style="background-color: grey;display: inline-block; margin-left: 10px;" type="button" onclick="cancel()">CANCEL</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="js/jquery.min.js"></script>


    <!-- Main JS-->
    <script src="js/global.js"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->