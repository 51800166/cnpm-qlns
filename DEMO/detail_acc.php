<?php 
    $r='';
    $acc_id ='';
    $acc_user ='';
    $acc_pass='';
    $acc_ro_id='';

    $low='';
    $medium='';
    $high='';
    $full='';

    $display_click_to_edit = "inline-block";
    $display_edit = "none";
    $display_back = "inline-block";
    $display_cancel = "none";
    require_once("ketnoi.php");
    if(isset($_GET['r'])){
        $r = $_GET['r'];
    }
    if(isset($_GET['key'])){
        $acc_id = $_GET['key'];

        $sql = "SELECT * FROM account WHERE acc_id='".$acc_id."'";
        $result = mysqli_query($conn,$sql);
        $show = mysqli_fetch_assoc($result);
        $acc_id = $show['acc_id'];
        $acc_user=$show['acc_user'];
        $acc_pass=$show['acc_pass'];

        $acc_ro_id=$show['acc_ro_id'];
        if($acc_ro_id=="1"){
            $low = "selected";
        }
        if($acc_ro_id=="2"){
            $medium ="selected";
        }
        if($acc_ro_id=="3"){
            $high = "selected";
        }
        if($acc_ro_id=="4"){
            $full = "selected";
        }
    }

 ?>
 <?php 
    if(isset($_GET['edit'])){
        echo $_GET['edit'];
        if($_GET['edit'] == "yes"){
            $disabled ='';
            $display_click_to_edit = "none";
            $display_edit = "inline-block";
            $display_back = "none";
            $display_cancel = "inline-block";
        }
    }
?>




<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">
    <title>Create Account</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="css/create-emp.css" rel="stylesheet" media="all">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<style type="text/css">
    .bg-dark {
          background-image: url("images/login_bg.jpg");
          background-size: cover;
}
</style>

<script type="text/javascript">
    
    function cancel(){
        window.location = "account_management.php?r=<?php echo $r?>&p=<?php echo $_GET['p'] ?>"
    } 
</script>
<body>
    <div class="page-wrapper bg-dark p-t-100 p-b-50">
        <div class="wrapper wrapper--w900">
            <div class="card card-6">
                <div class="card-heading">
                    <h2 class="title"  style="text-align: center;">ACCOUNT DETAIL</h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="edit_acc.php?r=<?php echo $r?>&key=<?php echo $acc_id; ?>&p=<?php echo $_GET['p'] ?>" enctype="multipart/form-data">

                        <div class="form-row">
                            <div class="name">Account ID <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="acc_id" min="1"
                                placeholder="" value="<?php echo $acc_id?>" required disabled>
                                <small class="label--desc">Can not null.</small>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">User Name <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="acc_user"
                                placeholder="" value="<?php echo $acc_user?>" required disabled>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Password <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="password" name="acc_pass"
                                placeholder="" value="<?php echo $acc_pass?>" required disabled>
                            </div>
                        </div> 

                        <div class="form-row">
                            <div class="name">Access Level <span style="color:red;">*</span></div>
                            <div class="value">

                                <select class="input--style-6" style="width: 100%; height: 40px" name="acc_ro_id" required disabled>
                                  <option></option>
                                  <option value="1" <?php echo $low;?>>Low access</option>
                                  <option value="2" <?php echo $medium;?>>Medium access</option>
                                  <option value="3" <?php echo $high;?>>High access</option>
                                  <option value="4" <?php echo $full;?>>Full access</option>
                                </select>
                                
                            </div>
                        </div>
                        
                        <div class="card-footer">
                            <button class="btn btn--radius-2 btn--blue-2 click_to_edit" id="click_to_edit" style="background: red;" name="edit" type="submit">CLICK TO EDIT</button>    
                            <button class="btn btn--radius-2 btn--blue-2" id="back" style="background-color: grey;" onclick="cancel()" type="button">BACK</button>
                        </div>

                    </form>
                </div>
                
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="js/jquery.min.js"></script>


    <!-- Main JS-->
    <script src="js/global.js"></script>

    <script>
        
        function click_to_edit(){
            document.getElementsByClassName("click_to_edit").innerHTML = "Hello";
        }
    </script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->