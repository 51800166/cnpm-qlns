<?php 

  require_once("ketnoi_add.php"); 
  $emp_atr=array("job_id","job_position","job_description","job_recruitment_status","job_recruitment_expected");
  
  $add=[];
  if(isset($_POST['submit'])){

        $stmt = $mysqli->prepare("INSERT INTO job (job_id,job_position,job_description,job_recruitment_status,job_recruitment_expected) VALUES (?,?,?,?,?)");
        $stmt->bind_param("isssi", $job_id, $job_position,$job_description,$job_recruitment_status,$job_recruitment_expected);
      
        if($_POST['job_id'] !=""){
          $job_id = $_POST['job_id'];
        }
        else{
          $job_id = NULL;
        }
        if($_POST['job_position'] !=""){
          $job_position = $_POST['job_position'];
        }
        else{
          $job_position = NULL;
        }
        if($_POST['job_description'] !=""){
          $job_description = $_POST['job_description'];
        }
        else{
          $job_description = NULL;
        }
        
        if($_POST['job_recruitment_status'] !=""){
          $job_recruitment_status = $_POST['job_recruitment_status'];
        }
        else{
          $job_recruitment_status = NULL;
        }
        if($_POST['job_recruitment_expected'] !=""){
          $job_recruitment_expected = $_POST['job_recruitment_expected'];
        }
        else{
          $job_recruitment_expected = 0;
        }
        
        $stmt->execute();
        header("Location: jobposition_management.php?r=".$_GET['r']."&p=".$_GET['p']);
   }


 ?>