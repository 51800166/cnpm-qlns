<?php 
require_once("ketnoi.php");
    $error = '';
    $user = "";
    $pwd = "";
    $s_user ="";
    $s_pwd = "";
   
    if( isset($_POST["acc_user"]) || isset($_POST["acc_pass"])){
        $user = $_POST["acc_user"];
        $sql = "SELECT acc_pass FROM account WHERE acc_user ='".$user."'";
        $result = mysqli_query($conn,$sql);
        $check = mysqli_fetch_assoc($result);
        if(!empty($check['acc_pass']) && password_verify($_POST['acc_pass'], $check['acc_pass']) == 1){
            header("Location: add_check_out.php?n=".$user);

            
        }
        else{
            $error = '<div class="form-row" id="error" style="background-color: red; padding:20px;">
                        <div class="name"></div>
                         <div class="value">
                            <div style="color: white; font-size: 20px;">TÀI KHOẢN HOẶC MẬT KHẨU KHÔNG ĐÚNG !!!</div>
                         </div>   
                        </div>';
                  $s_user = $_POST["acc_user"];
                  $s_pwd =  $_POST["acc_pass"];

        }
        
    }
  
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">
    <title>Check-out Station</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="css/create-emp.css" rel="stylesheet" media="all">
</head>
<style type="text/css">
    .bg-dark {
          background-image: url("images/login_bg.jpg");
          background-size: cover;
}
</style>

<script type="text/javascript">
    
    function reload(){
        window.location = "check_out.php";
    } 
</script>
<body>
    <div class="page-wrapper bg-dark p-t-100 p-b-50">
        <div class="wrapper wrapper--w900">
            <div class="card card-6">
                <div class="card-heading">
                    <h2 class="title"  style="text-align: center;">CHECK-OUT STATION</h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="#">

                        <div class="form-row">
                            <div class="name">User Name</div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="acc_user"
                                placeholder=" Enter your username to check" required value="<?php echo $s_user; ?>">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Password</div>
                            <div class="value">
                                <input class="input--style-6" type="password" name="acc_pass"
                                placeholder="Enter your password" required>
                            </div>
                        </div>
                        <?php 
                            echo $error;
                         ?>
                        <div class="card-footer">
                        <button class="btn btn--radius-2 btn--blue-2" type="submit" style="display: inline-block; margin-left: 0px;">CHECK OUT</button>
                
                        <button class="btn btn--radius-2 btn--blue-2" style="background-color: grey;display: inline-block; margin-left: 10px;" type="submit" onclick="reload()">RELOAD</button>
                </div>
                    </form>
                </div>
                
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="js/jquery.min.js"></script>


    <!-- Main JS-->
    <script src="js/global.js"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->