<?php 
  $role_check = '';
  $r='';
  if(isset($_GET['r'])){
    if($_GET['r'] == 4){
      $r=4;
      $p=$_GET['p'];
        $role_check ='<ul class="list-unstyled components mb-5">
                    <!-- EMPLOYEE MANAGEMENT -->
                    <li >
                      <a href="#em-Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">EMPLOYEE MANAGEMENT</a>
                      <ul class="collapse list-unstyled" id="em-Submenu">
                        <li>
                            <a href="employee_management.php?r='.$r.'&p='.$p.'">Employee List</a>
                        </li>
                        <li>
                            <a href="create_employee.php?r='.$r.'&p='.$p.'">Create New Profile</a>
                        </li>
                      </ul>
                    </li>
                    <!-- ACCOUNT MANAGEMENT -->
                    <li>
                      <a href="#am-Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">ACCOUNT MANAGEMENT</a>
                      <ul class="collapse list-unstyled" id="am-Submenu">
                        <li>
                            <a href="account_management.php?r='.$r.'&p='.$p.'">Account List</a>
                        </li>
                        <li>
                            <a href="create_account.php?r='.$r.'&p='.$p.'">Create New Account</a>
                        </li>
                      </ul>
                    </li>
                    <!-- CONTACT MANAGEMENT -->
                    <li>
                      <a href="#cm-Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">CONTRACT MANAGEMENT</a>
                      <ul class="collapse list-unstyled" id="cm-Submenu">
                        <li>
                            <a href="#">Contract List</a>
                        </li>
                        <li>
                            <a href="create_contract.php?r='.$r.'&p='.$p.'">Create New Contract</a>
                        </li>
                      </ul>
                    </li>
                    <!-- OFFICE MANAGEMENT -->
                    <li>
                      <a href="#om-Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">DEPARTMENT MANAGEMENT</a>
                      <ul class="collapse list-unstyled" id="om-Submenu">
                        <li>
                            <a href="office_management.php?r='.$r.'&p='.$p.'">Department List</a>
                        </li>
                        <li>
                            <a href="create_department.php?r='.$r.'&p='.$p.'">Create New Department</a>
                        </li>
                      </ul>
                    </li>

                    <!-- JOB POSITION MANAGEMENT -->
                    <li>
                      <a href="#jpm-Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">JOB POSITION MANAGEMENT</a>
                      <ul class="collapse list-unstyled" id="jpm-Submenu">
                        <li>
                            <a href="jobposition_management.php?r='.$r.'&p='.$p.'">JP List</a>
                        </li>
                        <li>
                            <a href="create_job.php?r='.$r.'&p='.$p.'">Create New JP</a>
                        </li>
                      </ul>
                    </li>

                    <!-- TIMEKEEPING MANAGEMENT -->
                    <li>
                      <a href="#tm-Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">ATTENDANCE MANAGEMENT</a>
                      <ul class="collapse list-unstyled" id="tm-Submenu">
                        <li>
                            <a href="timekeeping_management.php?r='.$r.'&p='.$p.'">Attendance List</a>
                        </li>
                        <li>
                            <a href="create_attendance.php?r='.$r.'&p='.$p.'">Create New Attendance</a>
                        </li>
                      </ul>
                    </li>

                    <!-- PLANNING MANAGEMENT -->
                    <li>
                      <a href="#apm-Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">PLANNING MANAGEMENT</a>
                      <ul class="collapse list-unstyled" id="apm-Submenu">
                        <li>
                            <a href="activityplan_management.php?r='.$r.'&p='.$p.'">Planning List</a>
                        </li>
                        <li>
                            <a href="create_planning.php?r='.$r.'&p='.$p.'">Create New Planning</a>
                        </li>
                      </ul>
                    </li>
                  </ul>';

    }
    if($_GET['r'] == 3){
      $r =3;
      $p = $_GET['p'];
      $role_check=  '<ul class="list-unstyled components mb-5">
                        <!-- EMPLOYEE MANAGEMENT -->
                        <li >
                          <a href="#em-Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">EMPLOYEE MANAGEMENT</a>
                          <ul class="collapse list-unstyled" id="em-Submenu">
                            <li>
                                <a href="employee_management.php?r='.$r.'&p='.$p.'">Employee List</a>
                            </li>
                            <li>
                                <a href="create_employee.php?r='.$r.'&p='.$p.'">Create New Profile</a>
                            </li>
                          </ul>
                        </li>
                        <!-- CONTACT MANAGEMENT -->
                        <li>
                          <a href="#cm-Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">CONTRACT MANAGEMENT</a>
                          <ul class="collapse list-unstyled" id="cm-Submenu">
                            <li>
                                <a href="#">Contract List</a>
                            </li>
                            <li>
                                <a href="create_contract.php?r='.$r.'&p='.$p.'">Create New Contract</a>
                            </li>
                          </ul>
                        </li>
                        <!-- OFFICE MANAGEMENT -->
                        <li>
                          <a href="#om-Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">DEPARTMENT MANAGEMENT</a>
                          <ul class="collapse list-unstyled" id="om-Submenu">
                            <li>
                                <a href="office_management.php?r='.$r.'&p='.$p.'">Department List</a>
                            </li>
                            <li>
                                <a href="create_department.php?r='.$r.'&p='.$p.'">Create New Department</a>
                            </li>
                          </ul>
                        </li>

                        <!-- JOB POSITION MANAGEMENT -->
                        <li>
                          <a href="#jpm-Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">JOB POSITION MANAGEMENT</a>
                          <ul class="collapse list-unstyled" id="jpm-Submenu">
                            <li>
                                <a href="jobposition_management.php?r='.$r.'&p='.$p.'">JP List</a>
                            </li>
                            <li>
                                <a href="create_job.php?r='.$r.'&p='.$p.'">Create New JP</a>
                            </li>
                          </ul>
                        </li>

                        <!-- TIMEKEEPING MANAGEMENT -->
                        <li>
                          <a href="#tm-Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">ATTENDANCE MANAGEMENT</a>
                          <ul class="collapse list-unstyled" id="tm-Submenu">
                            <li>
                                <a href="timekeeping_management.php?r='.$r.'&p='.$p.'">Attendance List</a>
                            </li>
                            <li>
                                <a href="create_attendance.php?r='.$r.'&p='.$p.'">Create New Attendance</a>
                            </li>
                          </ul>
                        </li>

                        <!-- ACTIVITY PLAN MANAGEMENT -->
                        <li>
                          <a href="#apm-Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">PLANNING MANAGEMENT</a>
                          <ul class="collapse list-unstyled" id="apm-Submenu">
                            <li>
                                <a href="activityplan_management.php?r='.$r.'&p='.$p.'">Planning List</a>
                            </li>
                            <li>
                                <a href="create_planning.php?r='.$r.'&p='.$p.'">Create New Planning</a>
                            </li>
                          </ul>
                        </li>
                      </ul>';
    }
    // if($_GET['r'] == 2){
    //     $role_check='<ul class="list-unstyled components mb-5">
    //                     <!-- TIMEKEEPING MANAGEMENT -->
    //                     <li>
    //                       <a href="#tm-Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">ATTENDANCE MANAGEMENT</a>
    //                       <ul class="collapse list-unstyled" id="tm-Submenu">
    //                         <li>
    //                             <a href="timekeeping_management.php?r='.$r.'">Attendance List</a>
    //                         </li>
    //                       </ul>
    //                     </li>

    //                     <!-- ACTIVITY PLAN MANAGEMENT -->
    //                     <li>
    //                       <a href="#apm-Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">PLANNING MANAGEMENT</a>
    //                       <ul class="collapse list-unstyled" id="apm-Submenu">
    //                         <li>
    //                             <a href="activityplan_management.php?r='.$r.'">Planning List</a>
    //                         </li>
    //                         <li>
    //                             <a href="create_planning.php?r='.$r.'">Create New Planning</a>
    //                         </li>
    //                       </ul>
    //                     </li>
    //       </ul>';
    // }
  }


 ?>
<?php 
  $row='';
 $emp_id ='';
 $start='';
 $end = '';
 $salary= '';
$status ='';
$name ='';
$con_id='';

  require_once("ketnoi.php");


    $sql = "SELECT * FROM contract";
    $result = mysqli_query($conn,$sql);
    $show = mysqli_fetch_all($result);
    for($i=0; $i<count($show); $i++){
         $con_id = $show[$i][0];
        //  $start = $show[$i][4];
        //  $start_change = date("d-m-Y", strtotime($start));
        //  $end = $show[$i][5];
        //  $end_change = date("d-m-Y", strtotime($end));
         $status = $show[$i][6];
         $emp_id = $show[$i][1];
         $con_job = $show[$i][2];
         $con_department = $show[$i][3];
         
         $sql_more = "SELECT * FROM job WHERE job_id ='".$con_job."'";
         $more = mysqli_query($conn,$sql_more);
         $show_more = mysqli_fetch_assoc($more);
         $name_job = $show_more['job_position'];
 
         $sql_more = "SELECT * FROM department WHERE dep_id ='".$con_department."'";
         $more = mysqli_query($conn,$sql_more);
         $show_more = mysqli_fetch_assoc($more);
         $name_dep = $show_more['dep_name'];
 
 
         $sql_more = "SELECT * FROM employee WHERE emp_id ='".$emp_id."'";
         $more = mysqli_query($conn,$sql_more);
         $show_more = mysqli_fetch_assoc($more);
         $name = $show_more['emp_name'];
         
        $sql = "SELECT con_id FROM contract WHERE con_archive ='0' AND con_id='".$con_id."'";
        $result = mysqli_query($conn,$sql);
        $check_archive = mysqli_fetch_assoc($result);
        if(!empty($check_archive)){

         $row.='<tr>
                     <th scope="row" style="text-align:center">'.$con_id.'</th>
                     <td>'.$name.'</td>
                     <td>'.$name_job.'</td>
                     <td>'.$name_dep.'</td>
                     <td>'.$status.'</td>
                     <td>
                       <div class="dropdown">
                             <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: black;">
                               <span style="color: yellow;">Action</span>
                             </button>
                             <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                               <a class="dropdown-item" href="delete_con.php?key='.$con_id.'&r='.$r.'&p='.$p.'">Delete</a>
                               <a class="dropdown-item" href="hide_con.php?key='.$con_id.'&r='.$r.'&p='.$p.'">Archive</a>
                               <a class="dropdown-item" href="detail_con.php?key='.$con_id.'&r='.$r.'&p='.$p.'">Detail</a>
                             </div>
                           </div>
                      </td>     
                 </tr>';   
        }  
        
    }
     

  
  

 

 ?>
<!doctype html>
<html lang="en">
  <head>
    <title>Contract Management</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style-main.css">
    <link rel="stylesheet" href="css\fontawesome-free-5.15.3-web\css\font-awesome.min.css">
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
  </head>
  <style type="text/css">
    
  </style>
  
  <body>
    
    <div class="wrapper d-flex align-items-stretch">
      <nav id="sidebar">
        <div class="p-4 pt-5">
          <a href="#" class="img logo rounded-circle mb-5" style="background-image: url(images/hr-img.jpg);"></a>
         <?php 
          echo $role_check;
          ?>


          <div class="footer">
            
        </div>
      </nav>
        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <div class="container-fluid">

            <button type="button" id="sidebarCollapse" class="btn btn-primary">
              <i class="fa fa-bars" id="bars"></i>
              <i class="fa fa-arrows-alt" id="arrows" style="display: none;"></i>
            </button>
            <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="nav navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="myprofile<?php echo $_GET['r'] ?>.php?p=<?php echo $p?>&r=<?php echo $_GET['r'] ?>"  style="color: white"><span id="h-item" style="margin-left: 10px;">My Profile</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login-page.php" style="color: white">
                      <span id="h-item" style="margin-left: 10px;">Log out</span>
                      <i class="fa fa-sign-out" id="h-item"></i>
                    </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        <div class="search-area">
          <input class="form-control" id="searchbar" type="text" placeholder="Search everything in list...">
          <i class="fa fa-search"></i>
        </div>

        <div class="list-emp">
          <table class="table table-list">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Employee Name</th>
                <th scope="col">Job Position</th>
                <th scope="col">Department</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody id="mydata">
             <?php 
              echo $row;

              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

  </body>
</html>