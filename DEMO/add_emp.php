<?php 

  require_once("ketnoi_add.php"); 
  $emp_atr=array("emp_id","acc_id","emp_name","emp_avatar","emp_gender","emp_phone","emp_email","emp_birth","emp_no_identification","emp_no_passport","emp_marital_status","emp_education_level","emp_education_field","emp_education_school","emp_manager","emp_coach");
  
  $add=[];
  if(isset($_POST['submit'])){

        $stmt = $mysqli->prepare("INSERT INTO employee(emp_id, acc_id,emp_name,emp_avatar,emp_gender,emp_phone,emp_email,emp_birth,emp_no_identification,emp_no_passport,emp_marital_status,emp_education_level,emp_education_field,emp_education_school,emp_manager,emp_coach) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param("iissssssssssssii", $emp_id, $acc_id,$emp_name,$emp_avatar,$emp_gender,$emp_phone,$emp_email,$emp_birth,$emp_no_identification,$emp_no_passport,$emp_marital_status,$emp_education_level,$emp_education_field,$emp_education_school,$emp_manager,$emp_coach);
      
        if($_POST['emp_id'] !=""){
          $emp_id = $_POST['emp_id'];
        }
        else{
          $emp_id = NULL;
        }
        if($_POST['acc_id'] !=""){
          $acc_id = $_POST['acc_id'];
        }
        else{
          $acc_id = NULL;
        }
        if($_POST['emp_name'] !=""){
          $emp_name = $_POST['emp_name'];
        }
        else{
          $emp_name = NULL;
        }
        if(isset($_FILES['emp_avatar'])&&$_FILES['emp_avatar']["name"]!=null){
            $emp_avatar = $_FILES['emp_avatar']["name"];
           //lấy tên của file:
            $file_name=$_FILES['emp_avatar']["name"];
            //lấy đường dẫn tạm lưu nội dung file:
            $file_tmp =$_FILES['emp_avatar']['tmp_name'];
            //tạo đường dẫn lưu file trên host:
            $path ="images/avatar/".$file_name;
            //upload nội dung file từ đường dẫn tạm vào đường dẫn vừa tạo:
            move_uploaded_file($file_tmp,$path);
        }
        else{
          $emp_avatar = NULL;
        }
        if($_POST['emp_gender'] !=""){
          $emp_gender = $_POST['emp_gender'];
        }
        else{
          $emp_gender = NULL;
        }
        if($_POST['emp_phone'] !=""){
          $emp_phone = $_POST['emp_phone'];
        }
        else{
          $emp_phone = NULL;
        }
        if($_POST['emp_email'] !=""){
          $emp_email = $_POST['emp_email'];
        }
        else{
          $emp_email = NULL;
        }
        if($_POST['emp_birth'] !=""){
          $emp_birth = $_POST['emp_birth'];
        }
        else{
          $emp_birth = NULL;
        }
        if($_POST['emp_no_identification'] !=""){
          $emp_no_identification = $_POST['emp_no_identification'];
        }
        else{
          $emp_no_identification = NULL;
        }
        if($_POST['emp_no_passport'] !=""){
          $emp_no_passport = $_POST['emp_no_passport'];
        }
        else{
          $emp_no_passport = NULL;
        }
        if($_POST['emp_marital_status'] !=""){
          $emp_marital_status = $_POST['emp_marital_status'];
        }
        else{
          $emp_marital_status = NULL;
        }
        if($_POST['emp_education_level'] !=""){
          $emp_education_level = $_POST['emp_education_level'];
        }
        else{
          $emp_education_level = NULL;
        }
        if($_POST['emp_education_field'] !=""){
          $emp_education_field = $_POST['emp_education_field'];
        }
        else{
          $emp_education_field = NULL;
        }
        if($_POST['emp_education_school'] !=""){
          $emp_education_school = $_POST['emp_education_school'];
        }
        else{
          $emp_education_school = NULL;
        }
        if($_POST['emp_manager'] !=""){
          $emp_manager = $_POST['emp_manager'];
        }
        else{
          $emp_manager = NULL;
        }
        if($_POST['emp_coach'] !=""){
          $emp_coach = $_POST['emp_coach'];
        }
        else{
          $emp_coach = NULL;
        }
        $stmt->execute();
        header("Location: employee_management.php?r=".$_GET['r']."&p=".$_GET['p']);
   }


 ?>