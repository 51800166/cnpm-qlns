<?php 
  $role_check = '';
  $r='';
  $p='';
  if(isset($_GET['r'])){
    $p = $_GET['p'];
  }


 ?>

<!doctype html>
<html lang="en">
  <head>
    <title>Account Management</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style-main.css">
    <link rel="stylesheet" href="css\fontawesome-free-5.15.3-web\css\font-awesome.min.css">
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
  </head>
  <style type="text/css">
    
  </style>
  
  <body>
    
    <div class="wrapper d-flex align-items-stretch">
      <nav id="sidebar">
        <div class="p-4 pt-5">
          <a href="#" class="img logo rounded-circle mb-5" style="background-image: url(images/hr-img.jpg);"></a>
          <?php 
            echo $role_check;

           ?>

          <div class="footer">
            
        </div>
      </nav>
        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <div class="container-fluid">

            <button type="button" id="sidebarCollapse" class="btn btn-primary">
              <i class="fa fa-bars" id="bars"></i>
              <i class="fa fa-arrows-alt" id="arrows" style="display: none;"></i>
            </button>
            <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="nav navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="myprofile<?php echo $_GET['r'] ?>.php?p=<?php echo $p?>&r=<?php echo $_GET['r'] ?>"  style="color: white"><span id="h-item" style="margin-left: 10px;">My Profile</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login-page.php" style="color: white">
                      <span id="h-item" style="margin-left: 10px;">Log out</span>
                      <i class="fa fa-sign-out" id="h-item"></i>
                    </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>

       
        <div class="list-emp">
          <p style="color: red; font-size: 21px;">NHÂN VIÊN THUỘC LEVEL 1 - LOW ACCESS KHÔNG ĐƯỢC TRUY CẬP CÁC CHỨC NĂNG NHÂN SỰ.</p>
        </div>
      </div>
    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

  </body>
</html>