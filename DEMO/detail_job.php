<?php 
    $r='';
    $job_id ='';
    $job_position ='';
    $job_description='';
    $job_recruitment_status='';
    $job_recruitment_expected='';

    $yes='';
    $no='';


    $display_click_to_edit = "inline-block";
    $display_edit = "none";
    $display_back = "inline-block";
    $display_cancel = "none";

    require_once("ketnoi.php");

    if(isset($_GET['r'])){
        $r = $_GET['r'];
    }
    if(isset($_GET['key'])){
        $job_id = $_GET['key'];

        $sql = "SELECT * FROM job WHERE job_id='".$job_id."'";
        $result = mysqli_query($conn,$sql);
        $show = mysqli_fetch_assoc($result);
        $job_id = $show['job_id'];
        $job_position=$show['job_position'];
        $job_description=$show['job_description'];
        
        $job_recruitment_expected=$show['job_recruitment_expected'];

        $job_recruitment_status=$show['job_recruitment_status'];
        if($job_recruitment_status=="Yes"){
            $yes = "selected";
        }
        if($job_recruitment_status=="No"){
            $no ="selected";
        }

    }

 ?>
 <?php 
    if(isset($_GET['edit'])){
        echo $_GET['edit'];
        if($_GET['edit'] == "yes"){
            $disabled ='';
            $display_click_to_edit = "none";
            $display_edit = "inline-block";
            $display_back = "none";
            $display_cancel = "inline-block";
        }
    }
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">
    <title>Job Detail</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="css/create-emp.css" rel="stylesheet" media="all">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<style type="text/css">
    .bg-dark {
          background-image: url("images/login_bg.jpg");
          background-size: cover;
}
</style>

<script type="text/javascript">
    
    function cancel(){
        window.location = "jobposition_management.php?r=<?php echo $r?>"
    } 
</script>
<body>
    <div class="page-wrapper bg-dark p-t-100 p-b-50">
        <div class="wrapper wrapper--w900">
            <div class="card card-6">
                <div class="card-heading">
                    <h2 class="title" style="text-align: center;">JOB DETAIL</h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="edit_job.php?r=<?php echo $r?>&key=<?php echo $job_id; ?>&p=<?php echo $_GET['p'] ?>" enctype="multipart/form-data">

                        <div class="form-row">
                            <div class="name">Job ID <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="job_id" min="1"
                                placeholder="" value="<?php echo $job_id?>" required disabled>
                                <small class="label--desc">Can not null.</small>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Job Position <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="job_position"
                                placeholder="Name of Job Position" value="<?php echo $job_position?>" required disabled>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Description</div>
                            <div class="value">
                                <textarea class="input--style-6" name="job_description" style="width: 100%" disabled><?php echo $job_description;?></textarea>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Status Recruitment <span style="color:red;">*</span></div>
                            <div class="value">

                                <select class="input--style-6" style="width: 100%; height: 40px" name="job_recruitment_status" required disabled>
                                  <option></option>
                                  <option value="Yes" <?php echo $yes;?> >Yes</option>
                                  <option value="No" <?php echo $no;?> >No</option>
                                </select>
                                
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Expected Number Of Recruitment</div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="job_recruitment_expected" min="1"
                                placeholder="" value="<?php echo $job_recruitment_expected?>" disabled>
                                <small class="label--desc">Choose 0 if you have not recruiment.</small>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button class="btn btn--radius-2 btn--blue-2 click_to_edit" id="click_to_edit" style="background: red;" name="edit" type="submit">CLICK TO EDIT</button>    
                            <button class="btn btn--radius-2 btn--blue-2" id="back" style="background-color: grey;" onclick="cancel()" type="button">BACK</button>
                        </div>
                    </form>
                </div>
                
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="js/jquery.min.js"></script>


    <!-- Main JS-->
    <script src="js/global.js"></script>

    <script>
        
        function click_to_edit(){
            document.getElementsByClassName("click_to_edit").innerHTML = "Hello";
        }
    </script>


</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->