<?php 
	require_once("ketnoi.php");
	$error = '';
	$user = "";
	$pwd = "";
	$s_user ="";
	$s_pwd = "";
	$role ="";
	$p ="";
	if( isset($_POST["username"]) || isset($_POST["password"])){
		$user = $_POST["username"];
		$sql = "SELECT acc_pass FROM account WHERE acc_user ='".$user."'";
		$result = mysqli_query($conn,$sql);
		$check = mysqli_fetch_assoc($result);
		if(!empty($check['acc_pass']) && password_verify($_POST['password'], $check['acc_pass']) == 1){
			$sql = "SELECT * FROM account WHERE acc_user ='".$user."'";
			$result = mysqli_query($conn,$sql);
			$check = mysqli_fetch_assoc($result);
			$role = $check['acc_ro_id'];
			$sql_emp_id = "SELECT emp_id FROM employee WHERE acc_id ='".$check['acc_id']."'";
			$result = mysqli_query($conn,$sql_emp_id);
			$check = mysqli_fetch_assoc($result);
			
			if($role >=3){
				header("Location: employee_management.php?r=".$role."&p=".$check['emp_id']);
			}
			if($role ==2){
				header("Location:timekeeping_management.php?r=".$role."&p=".$check['emp_id']);
			}
			if($role ==1){
				header("Location:for_low_access.php?r=".$role."&p=".$check['emp_id']);
			}
		}
		else{
			$error = '<div class="form-group" style=" margin-top:7px; padding: 7px; border-radius: 10px; background: #e84853; font-size: 12px;" id="error">
				    	<div>TÀI KHOẢN HOẶC MẬT KHẨU KHÔNG ĐÚNG !!!</div>
				  </div>';
				  $s_user = $_POST["username"];
				  $s_pwd =  $_POST["password"];

		}
		if($_POST["username"] == '' || $_POST["password"] == ''){
			$error = '<div class="form-group" style=" margin-top:7px; padding: 7px; border-radius: 10px; background: #e84853; font-size: 12px;" id="error">
				    	<div>TÀI KHOẢN HOẶC MẬT KHẨU ĐANG TRỐNG !!!</div>
				  </div>';
		}
		
	}
	

 ?>
 <?php 
 if(isset($_GET['err']) && $_GET['err'] ==1){
 	echo "<script>";
 	echo 'alert("Confirm Password Is Wrong !!!")';
 	echo "</script>";
 }

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Form</title>
	<link rel="stylesheet" href="css/style-login.css">
</head>
<style>
	body{
		background-image: url('images/login_bg_2.jpg');
		background-size: cover;
	}
</style>
<script>
	
	function login(){
		Windows.location = "check_login.php";
	}

	if(<?php $error?> > 0){
		document.getElementById('error').style.display = "block";
	}
</script>
<body>
<div class="container" id="container">
	<div class="form-container sign-up-container">
		<form class="register-form" action="register.php" method="POST">
			<h1>Create Account</h1>
			<br>
			<div class="form-group">
		   	 	<label for="username" id="rg-username">USERNAME</label>
		    	<input type="text" class="form-control" id="input-rg-username" placeholder="Enter New Username" name="res_user">
		  </div>
		  <div class="form-group">
		    	<label for="password" id="rg-password">PASSWORD</label>
		    	<input type="password" class="form-control" id="input-rg-pass" placeholder="Enter New Password" name="res_pass">
		  </div>
		  <div class="form-group">
		    	<label for="password" id="rg-cf-password">CONFIRM PASSWORD</label>
		    	<input type="password" class="form-control" id="input-rg-cf-pass" placeholder="Confirm Password" name="res_pass_conf">
		  </div>
			<br>
			<button class="rg-btn" type="submit">Register</button>
		</form>
	</div>
	<div class="form-container sign-in-container">
		<form class="login-form" action="#" method="POST">
			<h1>LOGIN IN</h1>
			<br>
			<div class="form-group">
		    	<label for="username" id="lg-username">USERNAME</label>
		    	<input type="text" class="form-control" id="input-lg-username" placeholder="Enter Username" name="username" value="<?php echo $s_user; ?>">
		  	</div>
		  <div class="form-group">
		    	<label for="password" id="lg-password">PASSWORD</label>
		    	<input type="password" class="form-control" id="input-lg-pass" placeholder="Enter Password" name="password" value="<?php echo $s_pwd; ?>">
		  </div>
		  <?php
				  echo $error; 
			?>  
			<a href="#" id="fgpass">Forgot your password?</a>
			<button class="lg-btn" type="submit">Log In</button>
		</form>
	</div>
	<div class="overlay-container">
		<div class="overlay">
			<div class="overlay-panel overlay-left">
				<h1 style="font-size: 53px;">Have an Account Already ?</h1>
				<p style="font-size: 26px;">Back to log in now</p>
				<button class="ghost" id="signIn">Back now !</button>
			</div>
			<div class="overlay-panel overlay-right">
				<h1 style="font-size: 56px">No Account ?</h1>
				<p style="font-size: 26px;">Click the under button to create right now</p>
				<button class="ghost" id="signUp">Register now !</button>
			</div>
		</div>
	</div>
</div>

</body>
<script src="js/script-login.js"></script>
</html>