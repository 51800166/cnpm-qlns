<?php 
    $r='';
    $pln_id ='';
    $pln_name ='';
    $pln_detail='';
    $pln_responsible_person='';

    $display_click_to_edit = "inline-block";
    $display_edit = "none";
    $display_back = "inline-block";
    $display_cancel = "none";

    require_once("ketnoi.php");

    if(isset($_GET['r'])){
        $r = $_GET['r'];
    }
    if(isset($_GET['key'])){
        $pln_id = $_GET['key'];

        $sql = "SELECT * FROM planning WHERE pln_id='".$pln_id."'";
        $result = mysqli_query($conn,$sql);
        $show = mysqli_fetch_assoc($result);

        $pln_id = $show['pln_id'];
        $pln_name=$show['pln_name'];
        $pln_detail=$show['pln_detail'];
        $pln_responsible_person=$show['pln_responsible_person'];
        
    }

 ?>
 <?php 
    if(isset($_GET['edit'])){
        echo $_GET['edit'];
        if($_GET['edit'] == "yes"){
            $disabled ='';
            $display_click_to_edit = "none";
            $display_edit = "inline-block";
            $display_back = "none";
            $display_cancel = "inline-block";
        }
    }
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">
    <title>Planning Detail</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="css/create-emp.css" rel="stylesheet" media="all">
</head>
<style type="text/css">
    .bg-dark {
          background-image: url("images/login_bg.jpg");
          background-size: cover;
}
</style>

<script type="text/javascript">
    
    function cancel(){
        window.location = "activityplan_management.php?r=<?php echo $r?>"
    } 
</script>
<body>
    <div class="page-wrapper bg-dark p-t-100 p-b-50">
        <div class="wrapper wrapper--w900">
            <div class="card card-6">
                <div class="card-heading">
                    <h2 class="title" style="text-align: center;">PLANNING DETAIL</h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="edit_pln.php?r=<?php echo $r?>&key=<?php echo $pln_id; ?>&p=<?php echo $_GET['p'] ?>" enctype="multipart/form-data">

                        <div class="form-row">
                            <div class="name">Planning ID <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="atd_id" min="1"
                                placeholder="" value="<?php echo $pln_id?>" required disabled>
                                <small class="label--desc">Can not null.</small>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Name <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="pln_name"
                                placeholder="Short name of project, task or plan" value="<?php echo $pln_name?>" required disabled>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Detail <span style="color:red;">*</span></div>
                            <div class="value">
                                <textarea class="input--style-6" name="pln_detail" style="width: 100%" required disabled><?php echo $pln_detail;?></textarea>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Responsible Person ID</div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="pln_responsible_person" min="1"
                                placeholder="" value="<?php echo $pln_responsible_person?>" disabled>
                                <small class="label--desc">Can null.</small>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn--radius-2 btn--blue-2 click_to_edit" id="click_to_edit" style="background: red;" name="edit" type="submit">CLICK TO EDIT</button>    
                            <button class="btn btn--radius-2 btn--blue-2" id="back" style="background-color: grey;" onclick="cancel()" type="button">BACK</button>
                        </div>
                    </form>
                </div>
                
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="js/jquery.min.js"></script>


    <!-- Main JS-->
    <script src="js/global.js"></script>

    <script>
        
        function click_to_edit(){
            document.getElementsByClassName("click_to_edit").innerHTML = "Hello";
        }
    </script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->