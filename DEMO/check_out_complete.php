<?php 

	require_once("ketnoi.php");
    date_default_timezone_set("Asia/Ho_Chi_Minh");
	$sql ="SELECT acc_id FROM account WHERE acc_user='".$_GET['n']."'";
	$result = mysqli_query($conn,$sql);
	$show = mysqli_fetch_assoc($result);
	$sql ="SELECT emp_name FROM employee WHERE acc_id='".$show['acc_id']."'";
	$result = mysqli_query($conn,$sql);
	$show = mysqli_fetch_assoc($result);

 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">
    <title>Check-out Station</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="css/create-emp.css" rel="stylesheet" media="all">
</head>
<style type="text/css">
    .bg-dark {
          background-image: url("images/login_bg.jpg");
          background-size: cover;
}
</style>

<script type="text/javascript">
    
    function reload(){
        window.location = "check_out.php";
    } 
</script>
<body>
    <div class="page-wrapper bg-dark p-t-100 p-b-50">
        <div class="wrapper wrapper--w900">
            <div class="card card-6">
                <div class="card-heading">
                    <h2 class="title"  style="text-align: center;">CHECK-OUT COMPLETE !!!</h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="check_out.php">

                         <div class="form-row" style="width:121%;">
                          
                            <div class="value">
                                <h2 style="color: green; text-align: center;">YOU HAVE ALREADY CHECKED OUT AT <?php echo date("H:i:s", strtotime($_GET['check_in'])); ?></h2>
                                 <h2 style="color: green; text-align: center;">See You Later, <?php echo $show['emp_name'] ?> </h2>
                            </div>
                        </div>
                        <div class="card-footer">
	                        <button class="btn btn--radius-2 btn--blue-2" type="submit">BACK</button>
	                
	                       
                		</div>
                    </form>
                </div>
                
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="js/jquery.min.js"></script>


    <!-- Main JS-->
    <script src="js/global.js"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->