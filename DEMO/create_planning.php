<?php 
    $r='';
    if(isset($_GET['r'])){
        $r = $_GET['r'];
    }
 ?>
 
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">
    <title>Create Planning</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="css/create-emp.css" rel="stylesheet" media="all">
</head>
<style type="text/css">
    .bg-dark {
          background-image: url("images/login_bg.jpg");
          background-size: cover;
}
</style>

<script type="text/javascript">
    
    function cancel(){
        window.location = "activityplan_management.php?r=<?php echo $r?>&p=<?php echo $_GET['p'] ?>"
    } 
</script>
<body>
    <div class="page-wrapper bg-dark p-t-100 p-b-50">
        <div class="wrapper wrapper--w900">
            <div class="card card-6">
                <div class="card-heading">
                    <h2 class="title" style="text-align: center;">CREATE PLANNING</h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="add_pln.php?r=<?php echo $r?>&p=<?php echo $_GET['p'] ?>" enctype="multipart/form-data">

                        <div class="form-row">
                            <div class="name">Name <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="pln_name"
                                placeholder="Short name of project, task or plan" required>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Detail <span style="color:red;">*</span></div>
                            <div class="value">
                                <textarea class="input--style-6" name="pln_detail" style="width: 100%" required></textarea>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Responsible Person ID</div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="pln_responsible_person" min="1"
                                placeholder="">
                                <small class="label--desc">Can null.</small>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn--radius-2 btn--blue-2" type="submit" name="submit" style="display: inline-block; margin-left: 0px;">CREATE</button>
                            <button class="btn btn--radius-2 btn--blue-2" style="background-color: grey; display: inline-block; margin-left: 10px;" type="submit" onclick="cancel()">CANCEL</button>
                        </div>
                    </form>
                </div>
                
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="js/jquery.min.js"></script>


    <!-- Main JS-->
    <script src="js/global.js"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->