<?php 

  require_once("ketnoi_add.php"); 
  $emp_atr=array("pln_name","pln_detail","pln_responsible_person");
  
  $add=[];
  if(isset($_POST['submit'])){

        $stmt = $mysqli->prepare("INSERT INTO planning (pln_name, pln_detail, pln_responsible_person) VALUES (?,?,?)");
        $stmt->bind_param("ssi", $pln_name, $pln_detail, $pln_responsible_person);
      
        if($_POST['pln_name'] !=""){
          $pln_name = $_POST['pln_name'];
        }
        else{
          $pln_name = NULL;
        }
        if($_POST['pln_detail'] !=""){
          $pln_detail = $_POST['pln_detail'];
        }
        else{
          $pln_detail = NULL;
        }

        if($_POST['pln_responsible_person'] !=""){
          $pln_responsible_person = $_POST['pln_responsible_person'];
        }
        else{
          $pln_responsible_person = NULL;
        }
        
        $stmt->execute();
        header("Location: activityplan_management.php?r=".$_GET['r']."&p=".$_GET['p']);
   }


 ?>