<?php 
    $r='';
    $emp_id ='';
    $acc_id ='';
    $emp_name='';
    $emp_phone='';
    $emp_mail='';
    $emp_birth='';
    $emp_no_identification='';
    $emp_avatar='';
    $emp_no_passport='';
    $emp_marital_status='';
    $emp_education_level='';
    $emp_education_field='';
    $emp_education_school='';
    $emp_manager='';
    $emp_coach='';
    $emp_gender='';


    $male='';
    $female='';
    $other='';


    $m ='';
    $d ='';
    $s = '';
    $w ='';
    $o ='';

    $graduate ='';
    $bachelor ='';
    $master ='';
    $doctor ='';
    $otheredu ='';

    require_once("ketnoi.php");
    if(isset($_GET['r'])){
        $r = $_GET['r'];
    }

    if(isset($_GET['p'])){
        $emp_id = $_GET['p'];

        $sql = "SELECT * FROM employee WHERE emp_id='".$emp_id."'";
        $result = mysqli_query($conn,$sql);
        $show = mysqli_fetch_assoc($result);
        $acc_id = $show['acc_id'];
        $emp_name=$show['emp_name'];
        $emp_phone=$show['emp_phone'];
        $emp_email=$show['emp_email'];
        $emp_birth=$show['emp_birth'];
        $emp_no_identification=$show['emp_no_identification'];
        $emp_avatar=$show['emp_avatar'];
        $emp_gender= $show['emp_gender'];
        if($emp_gender=="Male"){
            $male = "selected";
        }
        if($emp_gender=="Female"){
            $female ="selected";
        }
        if($emp_gender=="Other"){
            $other = "selected";
        }
        $emp_no_passport=$show['emp_no_passport'];
        $emp_marital_status=$show['emp_marital_status'];
        if($emp_marital_status=="Married"){
            $m = "selected";
        }
        if($emp_marital_status=="Divorced"){
            $d ="selected";
        }
        if($emp_marital_status=="Other"){
            $o = "selected";
        }
        if($emp_marital_status=="Single"){
            $s = "selected";
        }
        if($emp_marital_status=="Widower"){
            $w = "selected";
        }

        $emp_education_level=$show['emp_education_level'];
        if($emp_education_level=="Graduate"){
            $graduate = "selected";
        }
        if($emp_education_level=="Bachelor"){
            $bachelor ="selected";
        }
        if($emp_education_level=="Master"){
            $master = "selected";
        }
        if($emp_education_level=="Doctor"){
            $doctor = "selected";
        }
        if($emp_education_level=="Other"){
            $otheredu = "selected";
        }

        $emp_education_field=$show['emp_education_field'];
        $emp_education_school=$show['emp_education_school'];
        $emp_manager=$show['emp_manager'];
        $emp_coach=$show['emp_coach'];
    }
  

 ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">
    <title>My Profile</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="css/create-emp.css" rel="stylesheet" media="all">
</head>
<style type="text/css">
    .bg-dark {
          background-image: url("images/login_bg.jpg");
          background-size: cover;
}
</style>

<script type="text/javascript">
    
    function cancel(){
        window.location = "for_low_access.php?r=<?php echo $r?>&p=<?php echo $_GET['p'] ?>";
    } 

   
</script>
<body>
    <div class="page-wrapper bg-dark p-t-100 p-b-50">
        <div class="wrapper wrapper--w900">
            <div class="card card-6">
                <div class="card-heading">
                    <h2 class="title" style="text-align: center;">MY PROFILE</h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="update_emp.php?r=<?php echo $r?>&avatar=<?php echo $emp_avatar; ?>&p=<?php echo $_GET['p'] ?>" enctype="multipart/form-data">
                        <div class="form-row">
                            <div class="name">Employee ID <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="emp_id" min="0"
                                placeholder="" value="<?php echo $emp_id?>" required disabled>
                                <small class="label--desc">Can not null.</small>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Account ID</div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="acc_id" min="1"
                                placeholder="" value="<?php echo $acc_id?>" disabled >
                                <small class="label--desc">Can null.</small>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Full name <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="emp_name"
                                placeholder="Full name of employee"  value="<?php echo $emp_name?>"  required disabled>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Avatar</div>
                            <div class="value">
                                <div class="input-group js-input-file">
                                    <input class="input-file" type="file" name="emp_avatar" id="file" disabled >
                                    <label class="label--file" for="file">Choose file</label>
                                    <span class="input-file__info"><?php echo $emp_avatar;?></span>
                                </div>
                                <div class="label--desc">Upload your photo. Max file size 10 MB.</div>
                                <div class="label--desc">Don't need to add the same avatar.</div>
                            </div>
                        </div>

                       <div class="form-row">
                            <div class="name">Gender <span style="color:red;">*</span></div>
                            <div class="value">

                                <select class="input--style-6" style="width: 100%; height: 40px" name="emp_gender" required disabled>
                                  <option></option>
                                  <option value="Male" <?php echo $male;?> >Male</option>
                                  <option value="Female" <?php echo $female;?> >Female</option>
                                  <option value="Other" <?php echo $other;?> >Other</option>
                                </select>
                                
                            </div>
                        </div> 

                         <div class="form-row">
                            <div class="name">Phone <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="emp_phone"
                                placeholder="0000000000"  value="<?php echo $emp_phone; ?>" required disabled>
                            </div>
                        </div> 

                        <div class="form-row">
                            <div class="name">Email <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="Email" name="emp_email"
                                placeholder="nvawork@gmail.com"  value="<?php echo $emp_email; ?>" required disabled>
                            </div>
                        </div> 

                        <div class="form-row">
                            <div class="name">Date Of Birth <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="date" name="emp_birth" value="<?php echo $emp_birth; ?>" required disabled>
                            </div>
                        </div> 

                        <div class="form-row">
                            <div class="name">No. Identification</div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="emp_no_identification"
                                placeholder="" value="<?php echo $emp_no_identification; ?>" disabled >
                            </div>
                        </div> 
                        
                        <div class="form-row">
                            <div class="name">No. Passport</div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="emp_no_passport"
                                placeholder="" value="<?php echo $emp_no_passport; ?>" disabled >
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Marital Status</div>
                            <div class="value">

                                <select class="input--style-6" style="width: 100%; height: 40px" name="emp_marital_status" disabled >
                                  <option></option>
                                  <option value="Married" <?php echo $m;?> >Married</option>
                                  <option value="Divorced" <?php echo $d;?> >Divorced</option>
                                  <option value="Single" <?php echo $s;?> >Single</option>
                                  <option value="Widower" <?php echo $w;?> >Widower</option>
                                  <option value="Other" <?php echo $o;?> >Other</option>
                                </select>
                                
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Education Certificate Level</div>
                            <div class="value">

                                <select class="input--style-6" style="width: 100%; height: 40px" name="emp_education_level" disabled >
                                  <option></option>
                                  <option value="Graduate" <?php echo $graduate;?> >Graduate</option>
                                  <option value="Bachelor" <?php echo $bachelor;?> >Bachelor</option>
                                  <option value="Master" <?php echo $master;?> >Master</option>
                                  <option value="Doctor" <?php echo $doctor;?> >Doctor</option>
                                  <option value="Other" <?php echo $otheredu;?> >Other</option>
                                </select>
                                
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Education Field Of Study</div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="emp_education_field"
                                placeholder="" value="<?php echo $emp_education_field; ?>" disabled >
                            </div>
                        </div> 

                        <div class="form-row">
                            <div class="name">Education School</div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="emp_education_school"
                                placeholder="" value="<?php echo $emp_education_school; ?>" disabled >
                            </div>
                        </div> 

                        <div class="form-row">
                            <div class="name">Manager ID</div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="emp_manager" min="1"
                                placeholder="" value="<?php echo $emp_manager; ?>" disabled >
                                <small class="label--desc"></small>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Coach ID</div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="emp_coach" min="1"
                                placeholder="" value="<?php echo $emp_coach; ?>" disabled >
                                <small class="label--desc"></small>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn--radius-2 btn--blue-2" id="back" style="background-color: green;" onclick="cancel()" type="button">BACK</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="js/jquery.min.js"></script>


    <!-- Main JS-->
    <script src="js/global.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->