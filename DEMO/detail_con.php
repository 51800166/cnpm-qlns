<?php 
    $r='';
    $con_id ='';
    $emp_id ='';
    $con_job='';
    $con_department='';
    $con_start_day='';
    $con_end_day='';
    $con_status='';
    $con_note='';
    $con_wage_month='';



    $new='';
    $running='';
    $expired='';
    $cancelled='';

    $display_click_to_edit = "inline-block";
    $display_edit = "none";
    $display_back = "inline-block";
    $display_cancel = "none";
    require_once("ketnoi.php");
    if(isset($_GET['r'])){
        $r = $_GET['r'];
    }
    if(isset($_GET['key'])){
        $con_id = $_GET['key'];

        $sql = "SELECT * FROM contract WHERE con_id='".$con_id."'";
        $result = mysqli_query($conn,$sql);
        $show = mysqli_fetch_assoc($result);
        $con_id = $show['con_id'];
        $emp_id=$show['emp_id'];
        $con_job=$show['con_job'];
        $con_department=$show['con_department'];
        $con_start_day=$show['con_start_day'];
        $con_end_day=$show['con_end_day'];
        
        $con_note=$show['con_note'];
        $con_wage_month= $show['con_wage_month'];


        $con_status=$show['con_status'];
        if($con_status=="New"){
            $new = "selected";
        }
        if($con_status=="Running"){
            $running ="selected";
        }
        if($con_status=="Expired"){
            $expired = "selected";
        }
        if($con_status=="Cancelled"){
            $cancelled = "selected";
        }

    }

 ?>
 <?php 
    if(isset($_GET['edit'])){
        echo $_GET['edit'];
        if($_GET['edit'] == "yes"){
            $disabled ='';
            $display_click_to_edit = "none";
            $display_edit = "inline-block";
            $display_back = "none";
            $display_cancel = "inline-block";
        }
    }
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">
    <title>Contract Detail</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="css/create-emp.css" rel="stylesheet" media="all">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<style type="text/css">
    .bg-dark {
          background-image: url("images/login_bg.jpg");
          background-size: cover;
}
</style>

<script type="text/javascript">
    
    function cancel(){
        window.location = "contact_management.php?r=<?php echo $r?>&p=<?php echo $_GET['p'] ?>"
    } 
</script>
<body>
    <div class="page-wrapper bg-dark p-t-100 p-b-50">
        <div class="wrapper wrapper--w900">
            <div class="card card-6">
                <div class="card-heading">
                    <h2 class="title" style="text-align: center;">CONTRACT DETAIL</h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="edit_con.php?r=<?php echo $r?>&key=<?php echo $emp_id; ?>&p=<?php echo $_GET['p'] ?>" enctype="multipart/form-data">

                        <div class="form-row">
                            <div class="name">Contract ID <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="con_id" min="1"
                                placeholder="" value="<?php echo $con_id?>" required disabled>
                                <small class="label--desc">Can not null.</small>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Employee ID <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="emp_id" min="1"
                                placeholder="" value="<?php echo $emp_id?>"required disabled>
                                <small class="label--desc">Can not null.</small>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Job ID <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="con_job" min="1"
                                placeholder="" value="<?php echo $con_job?>"required disabled>
                                <small class="label--desc">Can not null.</small>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Department ID <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="con_department" min="1"
                                placeholder="" value="<?php echo $con_department?>"required disabled>
                                <small class="label--desc">Can not null.</small>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Start Date <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="date" name="con_start_day" value="<?php echo $con_start_day?>" required disabled>
                            </div>
                        </div> 

                        <div class="form-row">
                            <div class="name">End Date <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="date" name="con_end_day" value="<?php echo $con_end_day?>" required disabled>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Wage Per Month <span style="color:red;">*</span></div>
                            <div class="value">
                                <input class="input--style-6" type="number" name="con_wage_month"
                                placeholder="000000000" value="<?php echo $con_wage_month?>" required disabled>
                                <small class="label--desc">Unit of currency: VND (Viet Nam Dong)</small>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Status <span style="color:red;">*</span></div>
                            <div class="value">

                                <select class="input--style-6" style="width: 100%; height: 40px" name="con_status" value="<?php echo $con_status?>" required disabled>
                                  <option></option>
                                  <option value="New" <?php echo $new;?> >New</option>
                                  <option value="Running" <?php echo $running;?> >Running</option>
                                  <option value="Expired" <?php echo $expired;?> >Expired</option>
                                  <option value="Cancelled" <?php echo $cancelled;?> >Cancelled</option>
                                </select>
                                
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Note</div>
                            <div class="value">
                                <textarea class="input--style-6" name="con_note" style="width: 100%" value="<?php echo $con_note?>" disabled><?php echo $con_note;?></textarea>
                            </div>
                        </div> 
                        <div class="card-footer">
                            <button class="btn btn--radius-2 btn--blue-2 click_to_edit" id="click_to_edit" style="background: red;" name="edit" type="submit">CLICK TO EDIT</button>    
                            <button class="btn btn--radius-2 btn--blue-2" id="back" style="background-color: grey;" onclick="cancel()" type="button">BACK</button>
                        </div>
                    </form>
                </div>
               
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="js/jquery.min.js"></script>


    <!-- Main JS-->
    <script src="js/global.js"></script>

    <script>
        
        function click_to_edit(){
            document.getElementsByClassName("click_to_edit").innerHTML = "Hello";
        }
    </script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->