-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 09, 2021 lúc 05:27 PM
-- Phiên bản máy phục vụ: 10.4.18-MariaDB
-- Phiên bản PHP: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `hrdatabase`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `account`
--

CREATE TABLE `account` (
  `acc_id` bigint(20) NOT NULL,
  `acc_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acc_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acc_ro_id` bigint(20) NOT NULL,
  `acc_archive` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `account`
--

INSERT INTO `account` (`acc_id`, `acc_user`, `acc_pass`, `acc_ro_id`, `acc_archive`) VALUES
(1, 'npdong', '$2y$10$XXDRk4yButqAdOIcVHuPnehWToPeQPSWsnenao9v4c0VOUZTob9mu', 4, b'0'),
(2, 'hvnhut', '$2y$10$JO5UxYVhdCbAhz91PACDvupSX.EH8bbWtTNMJ5HvZiuQ8BgFdhacC', 4, b'0'),
(3, 'ntmhang', '$2y$10$J5mLAG2O0eSVGFV9Lv1F7OzKIqTuzyS8I4lWPwOR/lA2U5QDADC8S', 4, b'0'),
(4, 'ttthao', '$2y$10$xvtRNMxjpbSrg0kYnSxz..ZVsy6nr0vTBqNZa/EYz3zvP23KenDUq', 1, b'0'),
(5, 'ltphuong', '$2y$10$.D/hpUx/i5zIrYEk/Ad8qedhArmXFiVJIL/FCQ0V7Uf8kW2kwWmQy', 1, b'0'),
(6, 'htqgiang', '$2y$10$WO6.AHzWxe80wcDKgfQHyOQxMxGTcAp56aFOZdsafu1Y1vZUlbEN6', 1, b'0'),
(7, 'dtnlan', '$2y$10$6r.jSQvWB0eDMHVjNjLTce5x2crjsz4yoA4Qu5C2Dgjo55LNlyhsy', 1, b'0'),
(8, 'nttvan', '$2y$10$QjAI.pnjUjbi1bsRgfxZcex2l3FAdMaZ9P7Y0vZxgjqrsChSoIr.O', 1, b'0'),
(9, 'tthiep', '$2y$10$o4wCtxqTKLdjFc0B1pf2iuTX1gVnYOyR3AtA2G70d2UMvJ7vd/C0W', 2, b'0'),
(10, 'vtmthuy', '$2y$10$iJdb9KUL2x/sT8XJFs1sk.670LuuOekw9eO1OE.5zbeNaQE/U1olG', 2, b'0'),
(11, 'nvson', '$2y$10$rOH.jJecf0ZZGK92vEnqjuqk1d0xFcCQXfJ6S.OMzBhtK8aBrEIT2', 2, b'0'),
(12, 'dtthien', '$2y$10$0/ta8A2Ni6Z4L9bB1q3V6eLRLvQxG38SSA.ygGe7OnFrrHnU6D2XG', 2, b'0'),
(13, 'tvquang', '$2y$10$mYTnikQY4/JWPLHQsutxkuuEi8XA3K9ohhDiuXN6vYm00VyBvmVhq', 2, b'0'),
(14, 'bthuyen', '$2y$10$0OOdB7KeXH79FfxzfqC2Zu/RZ6n0ISmg.HXUaWbqQ9hskpRdntxte', 3, b'0'),
(15, 'ntxhuong', '$2y$10$EFI3kNTdmhvsCq1vip4dBuJ59tadNmgZXdPGT3vB2u5aZYlQvIgry', 3, b'0'),
(16, 'dtduong', '$2y$10$tWx7Wtcj18OGRJOxTjiJp.X32fHuk6h6NMMuTYdiZN4bqTq5wJ5V2', 3, b'0'),
(17, 'ntnhiep', '$2y$10$hsgs64xplZCyNESKJNwW8.vI0urXtdU0Xu0Wbakk9cI7H2G4.CN3e', 3, b'0'),
(18, 'ltttam', '$2y$10$7OzlSSwdTTvkNmM0/1K3HeJVJMcbK5tA9DedUKm.cHss/20O4fHZq', 3, b'0'),
(19, 'ptdquyen', '$2y$10$0DvuOQUoz4oEoF6ykoCMZ.GYaurhiQov73t4USxhyeC3hZ1MR1uam', 4, b'0'),
(20, 'ntmtinh', '$2y$10$hyoRKfCHp3QjCJJbGTQVf.EiAtvUQBZUBThyRupQbiX8VCkjKbnX.', 4, b'0');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `account_role`
--

CREATE TABLE `account_role` (
  `acc_ro_id` bigint(20) NOT NULL,
  `acc_ro_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `account_role`
--

INSERT INTO `account_role` (`acc_ro_id`, `acc_ro_name`) VALUES
(4, 'Full access'),
(3, 'High access'),
(1, 'Low access'),
(2, 'Medium access');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `attendance`
--

CREATE TABLE `attendance` (
  `atd_id` bigint(20) NOT NULL,
  `emp_id` bigint(20) NOT NULL,
  `atd_checkin` datetime NOT NULL,
  `atd_checkout` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `attendance`
--

INSERT INTO `attendance` (`atd_id`, `emp_id`, `atd_checkin`, `atd_checkout`) VALUES
(1, 1, '2021-04-26 14:39:00', '2021-04-27 19:38:14'),
(2, 2, '2021-04-26 14:39:19', '2021-04-27 19:38:19'),
(3, 3, '2021-04-26 14:39:40', '2021-04-27 19:38:22'),
(4, 4, '2021-04-26 19:36:15', '2021-04-27 19:38:25'),
(5, 5, '2021-04-26 19:36:51', '2021-04-27 19:38:27'),
(6, 6, '2021-04-26 19:36:58', '2021-04-27 19:38:35'),
(7, 7, '2021-04-26 19:37:52', '2021-04-27 19:38:32'),
(8, 8, '2021-04-26 19:36:38', '2021-04-27 19:38:39'),
(9, 9, '2021-04-26 19:37:05', '2021-04-27 19:38:43'),
(10, 10, '2021-04-26 19:37:08', '2021-04-27 19:39:18'),
(11, 11, '2021-04-26 19:37:11', '2021-04-27 19:39:15'),
(12, 12, '2021-04-26 19:37:16', '2021-04-27 19:39:13'),
(13, 13, '2021-04-26 19:37:18', '2021-04-27 19:39:09'),
(14, 14, '2021-04-26 19:37:21', '2021-04-27 19:39:07'),
(15, 15, '2021-04-26 19:37:24', '2021-04-27 19:39:02'),
(16, 16, '2021-04-26 19:37:27', '2021-04-27 19:38:58'),
(17, 17, '2021-04-26 19:37:30', '2021-04-27 19:38:56'),
(18, 18, '2021-04-26 19:37:35', '2021-04-27 19:38:53'),
(19, 19, '2021-04-26 19:37:46', '2021-04-27 19:38:51'),
(20, 20, '2021-04-26 19:37:49', '2021-04-27 19:38:47');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bank_account`
--

CREATE TABLE `bank_account` (
  `ban_id` bigint(20) NOT NULL,
  `ban_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ban_number` int(20) NOT NULL,
  `ban_holder` bigint(20) NOT NULL,
  `ban_holder_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `bank_account`
--

INSERT INTO `bank_account` (`ban_id`, `ban_name`, `ban_number`, `ban_holder`, `ban_holder_name`) VALUES
(1, 'VPBank', 124560520, 1, 'NGUYEN PHU DONG'),
(2, 'VPBank', 124560521, 2, 'HUYNH VAN NHU'),
(3, 'VPBank', 124560522, 3, 'NGUYEN THI MINH HANG'),
(4, 'VPBank', 124560523, 4, 'TRAN THI THANH HAO'),
(5, 'VPBank', 124560524, 5, 'LUONG THI PHUONG'),
(6, 'VPBank', 124560525, 6, 'HO THI QUYNH GIANG'),
(7, 'VPBank', 124560526, 7, 'DO THI NGOC LAN'),
(8, 'VPBank', 124560527, 8, 'NGUYEN THI THANH VAN'),
(9, 'VPBank', 124560528, 9, 'TA THI HIEP'),
(10, 'VPBank', 124560529, 10, 'VO THI MINH THUY'),
(11, 'VPBank', 124560530, 11, 'NGUYEN VIET SON'),
(12, 'VPBank', 124560531, 12, 'DO THI THANH TIEN'),
(13, 'VPBank', 124560532, 13, 'TRAN VAN QUANG'),
(14, 'VPBank', 124560533, 14, 'BUI THANH HUYEN'),
(15, 'VPBank', 124560534, 15, 'NGUYEN THI XUAN HUONG'),
(16, 'VPBank', 124560535, 16, 'DOAN THI DUONG'),
(17, 'VPBank', 124560536, 17, 'NGUYEN THUY NU HIEP'),
(18, 'VPBank', 124560537, 18, 'LE THI THU TAM'),
(19, 'VPBank', 124560538, 19, 'PHAN THI DIEM QUYEN'),
(20, 'VPBank', 124560539, 20, 'NGUYEN THI MY TINH');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contract`
--

CREATE TABLE `contract` (
  `con_id` bigint(20) NOT NULL,
  `emp_id` bigint(20) NOT NULL,
  `con_job` bigint(20) NOT NULL,
  `con_department` bigint(20) NOT NULL,
  `con_start_day` date NOT NULL,
  `con_end_day` date NOT NULL,
  `con_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `con_note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `con_wage_month` double NOT NULL,
  `con_archive` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `contract`
--

INSERT INTO `contract` (`con_id`, `emp_id`, `con_job`, `con_department`, `con_start_day`, `con_end_day`, `con_status`, `con_note`, `con_wage_month`, `con_archive`) VALUES
(1, 1, 1, 1, '2021-04-26', '2021-06-26', 'Running', NULL, 18000000, b'0'),
(2, 2, 1, 1, '2021-04-26', '2021-06-26', 'Running', NULL, 18000000, b'0'),
(3, 3, 1, 1, '2021-04-26', '2021-06-26', 'Running', NULL, 18000000, b'0'),
(4, 4, 1, 2, '2021-04-26', '2021-06-26', 'Running', NULL, 20000000, b'0'),
(5, 5, 1, 2, '2021-04-26', '2021-06-26', 'Running', NULL, 18000000, b'0'),
(6, 6, 1, 2, '2021-04-26', '2021-06-26', 'Running', NULL, 18000000, b'0'),
(7, 7, 1, 4, '2021-04-26', '2021-06-26', 'Running', NULL, 18000000, b'0'),
(8, 8, 1, 4, '2021-04-26', '2021-06-26', 'Running', NULL, 18000000, b'0'),
(9, 9, 1, 4, '2021-04-26', '2021-06-26', 'Expired', NULL, 19000000, b'0'),
(10, 10, 5, 2, '2021-04-26', '2021-05-26', 'New', NULL, 18000000, b'0'),
(11, 11, 5, 2, '2021-04-26', '2021-05-26', 'New', NULL, 43000002, b'0'),
(12, 12, 5, 4, '2021-04-26', '2021-05-26', 'New', NULL, 18000000, b'0'),
(13, 13, 5, 4, '2021-04-26', '2021-04-27', 'Cancelled', NULL, 23500000, b'0'),
(14, 14, 6, 1, '2021-04-26', '2021-06-26', 'Running', NULL, 18000000, b'0'),
(15, 15, 3, 2, '2021-04-26', '2021-06-26', 'Running', NULL, 18000000, b'0'),
(16, 16, 6, 3, '2021-04-26', '2021-06-26', 'Running', NULL, 18000000, b'0'),
(17, 17, 4, 4, '2021-04-26', '2021-06-26', 'Running', NULL, 18000000, b'0'),
(18, 18, 7, 5, '2021-04-26', '2021-06-26', 'Running', NULL, 18000300, b'0'),
(19, 19, 2, 5, '2021-04-20', '2021-06-26', 'Running', NULL, 18000000, b'0'),
(20, 20, 2, 5, '2021-04-26', '2021-06-26', 'Running', NULL, 18000000, b'0');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `department`
--

CREATE TABLE `department` (
  `dep_id` bigint(20) NOT NULL,
  `dep_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dep_manager` bigint(20) DEFAULT NULL,
  `dep_archive` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `department`
--

INSERT INTO `department` (`dep_id`, `dep_name`, `dep_manager`, `dep_archive`) VALUES
(1, 'Management', 14, b'0'),
(2, 'Professional Services', 15, b'0'),
(3, 'Research & Development', 16, b'0'),
(4, 'Sales', 17, b'0'),
(5, 'Information System', 18, b'0');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `employee`
--

CREATE TABLE `employee` (
  `emp_id` bigint(20) NOT NULL,
  `acc_id` bigint(20) DEFAULT NULL,
  `emp_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emp_avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_gender` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emp_phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emp_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emp_birth` date NOT NULL,
  `emp_no_identification` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_no_passport` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_marital_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_education_level` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_education_field` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_education_school` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_manager` bigint(20) DEFAULT NULL,
  `emp_coach` bigint(20) DEFAULT NULL,
  `emp_archive` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `employee`
--

INSERT INTO `employee` (`emp_id`, `acc_id`, `emp_name`, `emp_avatar`, `emp_gender`, `emp_phone`, `emp_email`, `emp_birth`, `emp_no_identification`, `emp_no_passport`, `emp_marital_status`, `emp_education_level`, `emp_education_field`, `emp_education_school`, `emp_manager`, `emp_coach`, `emp_archive`) VALUES
(1, 1, 'Nguyễn Phú Đồng', 'anh2.png', 'Male', '0944306162', 'npdwork@gmail.com', '2000-07-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(2, 2, 'Huỳnh Văn Nhứt', 'anh4.png', 'Male', '0341234532', 'hvnwork@gmail.com', '2000-08-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(3, 3, 'Nguyễn Thị Minh Hằng', 'anh5.png', 'Female', '0944306155', 'ntmhwork@gmail.com', '1999-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(4, 4, 'Trần Thị Thanh Hảo', 'anh6.png', 'Female', '0944356162', 'ttthwork@gmail.com', '2000-02-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(5, 5, 'Lương Thị Phương', 'anh7.png', 'Female', '0944306892', 'ltpwork@gmail.com', '1999-02-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(6, 6, 'Hồ Thị Quỳnh Giang', 'anh8.png', 'Female', '0986306162', 'htqgwork@gmail.com', '1998-03-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(7, 7, 'Đỗ Thị Ngọc Lan', 'anh9.png', 'Female', '0944375162', 'dtnlwork@gmail.com', '1996-10-06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(8, 8, 'Nguyễn Thị Thanh Vân', 'anh10.png', 'Female', '0944326162', 'nttvwork@gmail.com', '2000-01-02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(9, 9, 'Tạ Thị Hiệp', 'anh11.png', 'Female', '0944312162', 'tthwork@gmail.com', '2000-10-30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(10, 10, 'Võ Thị Minh Thủy', 'anh12.png', 'Female', '0634306162', 'vtmtwork@gmail.com', '1998-09-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(11, 11, 'Nguyễn Viết Sơn', 'anh13.png', 'Male', '0944378162', 'nvswork@gmail.com', '1998-05-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(12, 12, 'Đỗ Thị Thanh Tiền', 'anh14.png', 'Female', '0945606162', 'dtttwork@gmail.com', '2000-03-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(13, 13, 'Trần Văn Quang', 'anh15.png', 'Male', '0944266162', 'tvqwork@gmail.com', '1999-11-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(14, 14, 'Bùi Thanh Huyền', 'anh16.png', 'Male', '0944256162', 'bthwork@gmail.com', '1997-03-02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(15, 15, 'Nguyễn Thị Xuân Hương', 'anh17.png', 'Female', '0944308962', 'ntxhwork@gmail.com', '2000-02-27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(16, 16, 'Đoàn Thi Đương', 'anh18.png', 'Male', '0944302662', 'dtdwork@gmail.com', '1999-12-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(17, 17, 'Nguyễn Thuỳ Nữ Hiệp', 'anh19.png', 'Male', '0944786162', 'ntnhwork@gmail.com', '1995-02-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(18, 18, 'Lê Thị Thu Tâm', 'anh20.png', 'Female', '0996306162', 'ltttwork@gmail.com', '1994-10-09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(19, 19, 'Phan Thị Diễm Quyên', 'anh3.png', 'Female', '0948506162', 'ptdqwork@gmail.com', '2000-06-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
(20, 20, 'Nguyễn Thi Mỹ Tính', 'anh1.png', 'Male', '0944966162', 'ntmtwork@gmail.com', '2000-07-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `job`
--

CREATE TABLE `job` (
  `job_id` bigint(20) NOT NULL,
  `job_position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_recruitment_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_recruitment_expected` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `job`
--

INSERT INTO `job` (`job_id`, `job_position`, `job_description`, `job_recruitment_status`, `job_recruitment_expected`) VALUES
(1, 'Consultant', 'Customers advise on products, services and companies to prioritize policies.', 'Yes', 10),
(2, 'Experienced Developer', 'Make policies and development plans for the company.\r\n', 'Yes', 5),
(3, 'Human Resources Manager', 'Managing corporate personnel matters.\r\n', 'Yes', 1),
(4, 'Marketing and Community Manager', 'Plan and execute advertising campaigns to further develop the company\'s image.\r\n', 'Yes', 1),
(5, 'Trainee', 'Complete the work assigned by the manager.\r\n', 'Yes', 20),
(6, 'Chief Executive Officer', 'Responsible for operating all activities of the company\r\n', 'No', 0),
(7, 'Chief Technical Officer', 'Responsible for the maintenance of equipment.\r\n', 'No', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `planning`
--

CREATE TABLE `planning` (
  `pln_id` bigint(20) NOT NULL,
  `pln_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pln_detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pln_responsible_person` bigint(20) DEFAULT NULL,
  `pln_archive` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `planning`
--

INSERT INTO `planning` (`pln_id`, `pln_name`, `pln_detail`, `pln_responsible_person`, `pln_archive`) VALUES
(1, 'Setup IT Materials', 'What will this training manual or set of training materials teach people to do?You might think the answer is obvious, but it’s imperative to identify your exact goals. ', NULL, b'0'),
(2, 'Plan Training', 'A training session plan – also called a learning plan – is an organized description of the activities and resources you\'ll use to guide a group toward a specific learning objective.', NULL, b'0'),
(3, 'Training', 'Although there are many categories of training such as management training and or sales training, employees with Project Management skills are an important asset to any organisation.', NULL, b'0'),
(4, 'Compute Out Delais', 'When a document is to be served or filed within a defined number of days before or after a specified event, do not include the day of the event when calculating the deadline', NULL, b'0'),
(5, 'Take Back HR Materials', 'Training materials, such as job aids and training manuals, are grouped by specific HR processing topics.', NULL, b'0');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`acc_id`),
  ADD UNIQUE KEY `acc_user` (`acc_user`),
  ADD KEY `FK_account` (`acc_ro_id`);

--
-- Chỉ mục cho bảng `account_role`
--
ALTER TABLE `account_role`
  ADD PRIMARY KEY (`acc_ro_id`),
  ADD UNIQUE KEY `acc_ro_name` (`acc_ro_name`);

--
-- Chỉ mục cho bảng `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`atd_id`),
  ADD KEY `FK_attendance` (`emp_id`);

--
-- Chỉ mục cho bảng `bank_account`
--
ALTER TABLE `bank_account`
  ADD PRIMARY KEY (`ban_id`),
  ADD UNIQUE KEY `ban_number` (`ban_number`),
  ADD KEY `FK_bank_account` (`ban_holder`);

--
-- Chỉ mục cho bảng `contract`
--
ALTER TABLE `contract`
  ADD PRIMARY KEY (`con_id`),
  ADD KEY `FK_contract_2` (`con_job`),
  ADD KEY `FK_contract_3` (`con_department`),
  ADD KEY `FK_contract_1` (`emp_id`) USING BTREE;

--
-- Chỉ mục cho bảng `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`dep_id`),
  ADD UNIQUE KEY `dep_name` (`dep_name`);

--
-- Chỉ mục cho bảng `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`emp_id`),
  ADD UNIQUE KEY `emp_no_identification` (`emp_no_identification`),
  ADD UNIQUE KEY `emp_no_passport` (`emp_no_passport`),
  ADD UNIQUE KEY `acc_id` (`acc_id`),
  ADD KEY `FK_employee_2` (`emp_manager`),
  ADD KEY `FK_employee_3` (`emp_coach`);

--
-- Chỉ mục cho bảng `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`job_id`);

--
-- Chỉ mục cho bảng `planning`
--
ALTER TABLE `planning`
  ADD PRIMARY KEY (`pln_id`),
  ADD KEY `FK_planning` (`pln_responsible_person`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `attendance`
--
ALTER TABLE `attendance`
  MODIFY `atd_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `bank_account`
--
ALTER TABLE `bank_account`
  MODIFY `ban_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `planning`
--
ALTER TABLE `planning`
  MODIFY `pln_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `account`
--
ALTER TABLE `account`
  ADD CONSTRAINT `FK_account` FOREIGN KEY (`acc_ro_id`) REFERENCES `account_role` (`acc_ro_id`);

--
-- Các ràng buộc cho bảng `attendance`
--
ALTER TABLE `attendance`
  ADD CONSTRAINT `FK_attendance` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`);

--
-- Các ràng buộc cho bảng `bank_account`
--
ALTER TABLE `bank_account`
  ADD CONSTRAINT `FK_bank_account` FOREIGN KEY (`ban_holder`) REFERENCES `employee` (`emp_id`);

--
-- Các ràng buộc cho bảng `contract`
--
ALTER TABLE `contract`
  ADD CONSTRAINT `FK_contract_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`),
  ADD CONSTRAINT `FK_contract_2` FOREIGN KEY (`con_job`) REFERENCES `job` (`job_id`),
  ADD CONSTRAINT `FK_contract_3` FOREIGN KEY (`con_department`) REFERENCES `department` (`dep_id`);

--
-- Các ràng buộc cho bảng `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `FK_employee_1` FOREIGN KEY (`acc_id`) REFERENCES `account` (`acc_id`),
  ADD CONSTRAINT `FK_employee_2` FOREIGN KEY (`emp_manager`) REFERENCES `employee` (`emp_id`),
  ADD CONSTRAINT `FK_employee_3` FOREIGN KEY (`emp_coach`) REFERENCES `employee` (`emp_id`);

--
-- Các ràng buộc cho bảng `planning`
--
ALTER TABLE `planning`
  ADD CONSTRAINT `FK_planning` FOREIGN KEY (`pln_responsible_person`) REFERENCES `employee` (`emp_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
